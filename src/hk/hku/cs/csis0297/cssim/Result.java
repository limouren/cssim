package hk.hku.cs.csis0297.cssim;

import hk.hku.cs.csis0297.cssim.domain.Customer;
import hk.hku.cs.csis0297.cssim.domain.Queue;
import hk.hku.cs.csis0297.cssim.domain.Staff;
import hk.hku.cs.csis0297.cssim.log.Log;

import java.util.ArrayList;

public class Result {
	private ArrayList<ResultRecord> resultRecords;
	private int maxMainQueueLength;
	private int maxFoodDrinkQueueLength;

	private double averageServiceTime[];
	private int longestServiceTime;

	private double averagePercentIdleTime[];

	private boolean isNoError; // to indicate if any queue exceeded the limit
	private int simulationStartTime;
	private int simulationPause;
	private int simulationEndTime;

	private Log log;

	public Result() {
		resultRecords = new ArrayList<ResultRecord>();

		maxMainQueueLength = 0;
		maxFoodDrinkQueueLength = 0;

		averageServiceTime = new double[Customer.NUM_OF_ORDER_CATEGORY];
		for (int i = 0; i < Customer.NUM_OF_ORDER_CATEGORY; i++)
			averageServiceTime[i] = 0;

		longestServiceTime = 0;

		averagePercentIdleTime = new double[Queue.NUM_OF_STAFF_CATEGORY];
		for (int i = 0; i < Queue.NUM_OF_STAFF_CATEGORY; i++)
			averagePercentIdleTime[i] = 0;

		setNoError(true);
	}

	public void generateResult(ArrayList<Customer> customers, Queue[] queue,
			int startTime, int endTime, int pause, int queueLimit, Log log) {

		ArrayList<Customer> simulationCustomers = new ArrayList<Customer>();

		simulationStartTime = startTime;
		simulationPause = pause;
		simulationEndTime = endTime; // real simulation end time, which is
										// subject to queue length

		for (int i = startTime; i <= endTime; i++) {
			int mainQueueCount = 0;
			int foodDrinkQueueCount = 0;

			for (Customer tempCustomer : customers) {
				if (tempCustomer.getArrivalTime(Queue.CASHIER) != Customer.NO_RESULT
						&& i >= tempCustomer.getArrivalTime(Queue.CASHIER)
						&& (i < tempCustomer
								.getQueueStartEntertainTime(Queue.CASHIER) || tempCustomer
								.getQueueStartEntertainTime(Queue.CASHIER) == Customer.NO_RESULT))
					mainQueueCount++;
			}

			for (Customer tempCustomer : customers) {
				if (tempCustomer.foodDrinkQueueArrivalTime() != Customer.NO_RESULT
						&& i >= tempCustomer.foodDrinkQueueArrivalTime()
						&& (i < tempCustomer.foodDrinkQueueLeaveTime() || tempCustomer
								.foodDrinkQueueLeaveTime() == Customer.NO_RESULT))
					foodDrinkQueueCount++;
			}

			resultRecords.add(new ResultRecord(i, mainQueueCount,
					foodDrinkQueueCount));

			if (mainQueueCount > queueLimit || foodDrinkQueueCount > queueLimit) {
				setNoError(false);
				simulationEndTime = i; // set the simulation "stops" at this
										// time
				break; // "stop" the simulation after exceeding the queue limit
			}

		}

		/* Filter out unwanted customer for computing correct statistic - START */
		for (Customer tempCustomer : customers)
			if (tempCustomer.getArrivalTime(Queue.CASHIER) <= simulationEndTime)
				simulationCustomers.add(tempCustomer);
		/* Filter out unwanted customer for computing correct statistic - END */

		log.filterLogRecord(simulationEndTime);
		computeStat(simulationCustomers, queue);

		this.log = log;
	}

	protected void computeStat(ArrayList<Customer> customers, Queue[] queue) {
		computeMaxMainQueueLength();
		computeMaxFoodDrinkQueueLength();
		computeLongestServiceTime(customers);
		computeAverageServiceTime(customers);
		computeAveragePercentIdleTime(queue);
	}

	private void computeMaxMainQueueLength() {
		maxMainQueueLength = 0;
		for (ResultRecord record : resultRecords)
			if (record.getMainQueueLength() > maxMainQueueLength)
				maxMainQueueLength = record.getMainQueueLength();
	}

	private void computeMaxFoodDrinkQueueLength() {
		maxFoodDrinkQueueLength = 0;
		for (ResultRecord record : resultRecords)
			if (record.getFoodDrinkQueueLength() > maxFoodDrinkQueueLength)
				maxFoodDrinkQueueLength = record.getFoodDrinkQueueLength();
	}

	private void computeAverageServiceTime(ArrayList<Customer> customers) {
		for (int i = 0; i < Customer.NUM_OF_ORDER_CATEGORY; i++) {
			int totalServiceTime = 0;
			int customerCount = 0;

			for (Customer tempCustomer : customers) {
				if (tempCustomer.leaveShopTime() <= simulationEndTime
						&& tempCustomer.leaveShopTime() != Customer.NO_RESULT
						&& tempCustomer.getOrderType() == i) {
					totalServiceTime += tempCustomer.serviceTime();
					customerCount++;
				}
			}
			averageServiceTime[i] = 1.0 * totalServiceTime / customerCount;
		}

	}

	private void computeLongestServiceTime(ArrayList<Customer> customers) {
		longestServiceTime = 0;
		for (Customer tempCustomer : customers)
			if (tempCustomer.leaveShopTime() <= simulationEndTime
					&& tempCustomer.serviceTime() > longestServiceTime)
				longestServiceTime = tempCustomer.serviceTime();
	}

	private void computeAveragePercentIdleTime(Queue[] queue) {
		int totalTime = simulationEndTime - simulationStartTime;

		for (int i = 0; i < Queue.NUM_OF_STAFF_CATEGORY; i++) {
			int idleTimeCount = 0;
			for (Staff tempStaff : queue[i].getStaffList())
				idleTimeCount += tempStaff.getIdleTime(simulationEndTime);

			averagePercentIdleTime[i] = 1.0 * idleTimeCount / totalTime;
		}
	}

	public ArrayList<ResultRecord> getAllRecordList() {
		return resultRecords;
	}

	public ArrayList<ResultRecord> getResultRecordList() {
		ArrayList<ResultRecord> filteredRecords = new ArrayList<ResultRecord>();

		for (ResultRecord resultRow : resultRecords) {
			if ((resultRow.getTime() - simulationStartTime) % simulationPause == 0)
				filteredRecords.add(resultRow);
		}
		if ((simulationEndTime - simulationStartTime) % simulationPause != 0)
			filteredRecords.add(resultRecords.get(resultRecords.size() - 1));
		return filteredRecords;
	}

	public int getMaxMainQueueLength() {
		return maxMainQueueLength;
	}

	public int getMaxFoodDrinkQueueLength() {
		return maxFoodDrinkQueueLength;
	}

	public double[] getAverageServiceTime() {
		return averageServiceTime;
	}

	public int getLongestServiceTime() {
		return longestServiceTime;
	}

	public double[] getAveragePercentIdleTime() {
		return averagePercentIdleTime;
	}

	public boolean isNoError() {
		return isNoError;
	}

	public void setNoError(boolean isNoError) {
		this.isNoError = isNoError;
	}

	public int getSimulationEndTime() {
		return simulationEndTime;
	}

	public Log getLog() {
		return log;
	}

	public void setLog(Log log) {
		this.log = log;
	}
}
