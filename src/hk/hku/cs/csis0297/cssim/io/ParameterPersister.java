package hk.hku.cs.csis0297.cssim.io;

import hk.hku.cs.csis0297.cssim.ParameterSet;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.io.OutputStream;

/**
 * Persister to handle the persistence of ParameterSet.
 * 
 * This simple implementation directly serializes the ParameterSet as a object
 * files. Future implementation could persist the parameter as a editable text
 * file for superior usability.
 * 
 */
public class ParameterPersister {
	public static final String OUTPUT_FILE_PATH = "parameters.obj";

	public ParameterPersister() {
		// do nothing
	}

	public ParameterSet read() throws FileNotFoundException {
		try {
			InputStream buffer = new BufferedInputStream(new FileInputStream(
					OUTPUT_FILE_PATH));
			ObjectInput input = new ObjectInputStream(buffer);
			try {
				return (ParameterSet) input.readObject();
			} finally {
				input.close();
			}
		} catch (ClassNotFoundException e) {
			throw new RuntimeException(String.format(
					"Class not found while reading object of type %s",
					ParameterSet.class.getName()), e);
		} catch (FileNotFoundException e) {
			throw e;
		} catch (IOException e) {
			// fail hard for other IOException
			throw new RuntimeException(String.format("IOException occurred while reading from %s", OUTPUT_FILE_PATH), e);
		}
	}

	public void write(ParameterSet parameterSet) throws IOException {
		try {
			OutputStream buffer = new BufferedOutputStream(
					new FileOutputStream(OUTPUT_FILE_PATH));
			ObjectOutput output = new ObjectOutputStream(buffer);
			try {
				output.writeObject(parameterSet);
			} finally {
				output.close();
			}
		} catch (IOException e) {
			throw new IOException(String.format(
					"Exception thrown while writeing to file %s",
					OUTPUT_FILE_PATH), e);
		}
	}
}
