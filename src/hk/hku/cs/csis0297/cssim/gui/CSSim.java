package hk.hku.cs.csis0297.cssim.gui;

import hk.hku.cs.csis0297.cssim.ParameterSet;
import hk.hku.cs.csis0297.cssim.Result;
import hk.hku.cs.csis0297.cssim.Simulator;
import hk.hku.cs.csis0297.cssim.domain.Customer;
import hk.hku.cs.csis0297.cssim.domain.Queue;

import java.awt.EventQueue;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

public class CSSim {
	public static final String LOG_FILENAME_STRING_FORMAT = "simulation.%s.log";
	public static final DateFormat LOG_FILENAME_DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd'T'HHmmss");

	protected Simulator simulator = new Simulator();
	protected ParameterPanel parameterPanel;

	private JFrame frmCssim;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					CSSim window = new CSSim();
					window.frmCssim.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public CSSim() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmCssim = new JFrame();
		frmCssim.setTitle("CSSim");
		frmCssim.setBounds(0, 0, 450, 460);
		frmCssim.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmCssim.getContentPane().setLayout(null);

		parameterPanel = new ParameterPanel(this);
		switchPanel(parameterPanel);
	}

	protected void onProceedButtonClick() {
		ParameterSet parameterSet = parameterPanel.getParameterSet();

		// validation start
		// 1. check if order pattern adds up to 100
		int sum = 0;
		for (int i : parameterSet.orderPattern) {
			sum += i;
		}
		if (sum != 100) {
			JOptionPane.showMessageDialog(frmCssim,
					"Customer Distribution should add up to 100",
					"Validation Error", JOptionPane.WARNING_MESSAGE);
			return;
		}

		// 2. check if start time earlier than end time
		if (parameterSet.startTime >= parameterSet.endTime) {
			JOptionPane.showMessageDialog(frmCssim,
					"Simulation Start Time should should be earlier than End Time.",
					"Validation Error", JOptionPane.WARNING_MESSAGE);
			return;
		}

		switchPanel(new ConfirmationPanel(this, parameterSet));
	}

	protected void onSimulationButtonClick() {
		ParameterSet parameterSet = parameterPanel.getParameterSet();
		
		// transform the parameters to what the backend required
        parameterSet.orderPattern[Customer.DRINK] += parameterSet.orderPattern[Customer.PREPACKED_ITEM];
        parameterSet.orderPattern[Customer.FOOD] += parameterSet.orderPattern[Customer.DRINK];
        parameterSet.orderPattern[Customer.FOOD_AND_DRINK] += parameterSet.orderPattern[Customer.FOOD];
		
		Result result = simulator.run(parameterSet);
		
		// show dialog if needed
		if (!result.isNoError()) {
			JOptionPane.showMessageDialog(frmCssim, "Customers in queue exceeded limits");
		}
		
		// prepare and show result panel
		ResultPanel resultPanel = new ResultPanel(this, result);
		switchPanel(resultPanel);

		// write the log file
		writeResultAsFile(result);
	}
	
	protected void onBackButtonClick() {
		switchPanel(parameterPanel);
	}
	
	protected void onRestartButtonClick() {
		switchPanel(parameterPanel);
	}
	
	protected void switchPanel(JPanel panel) {
		frmCssim.getContentPane().removeAll();

		frmCssim.getContentPane().add(panel);
		frmCssim.invalidate();
		
		panel.setBounds(0, 0, 432, 410);
		
		frmCssim.validate();
		frmCssim.repaint();
	}
	
	protected boolean writeResultAsFile(Result result) {
		String filename = String.format(LOG_FILENAME_STRING_FORMAT, LOG_FILENAME_DATE_FORMAT.format(new Date()));
		
		FileWriter fileWriter = null;
		try {
			fileWriter = new FileWriter(filename);
		} catch (IOException e) {
			return false;
		}

		PrintWriter writer = null;
		try {
			writer = new PrintWriter(new BufferedWriter(fileWriter));

			writer.println("Time,CustomerID,Event");
			result.getLog().write(writer);

			writer.println();
			writer.println("Statistics");
			writer.println("Maximum length of the main customer queue: " + result.getMaxMainQueueLength());
			writer.println("maximum number of customers waiting for food and drinks: " + result.getMaxFoodDrinkQueueLength());
			writer.println("Average service time of customer who buy");
			writer.println("\ta prepacked item: " + result.getAverageServiceTime()[Customer.PREPACKED_ITEM] + "min");
			writer.println("\ta custom-made drink: " + result.getAverageServiceTime()[Customer.DRINK] + "min");
			writer.println("\ta food item: " +  + result.getAverageServiceTime()[Customer.FOOD] + "min");
			writer.println("\tboth drink and food item: " +  + result.getAverageServiceTime()[Customer.FOOD_AND_DRINK] + "min");
			writer.println("Longest service time experienced by a customer: " + result.getLongestServiceTime() + "min");
			writer.println("Average percent idle time for");
			writer.println("\tCashier: " + result.getAveragePercentIdleTime()[Queue.CASHIER] + "%");
			writer.println("\tBarista: " + result.getAveragePercentIdleTime()[Queue.BARISTA] + "%");
			writer.println("\tCook: " + result.getAveragePercentIdleTime()[Queue.COOK] + "%");
		} finally {
			if (writer != null) {
				writer.close();
			}
		}

		return true;
	}
}
