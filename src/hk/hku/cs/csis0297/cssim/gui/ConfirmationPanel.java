package hk.hku.cs.csis0297.cssim.gui;

import hk.hku.cs.csis0297.cssim.ParameterSet;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JPanel;

@SuppressWarnings("serial")
public class ConfirmationPanel extends JPanel {

	CSSim parent;
	private JPanel parameterPanel;
	
	/**
	 * Create the panel.
	 */
	public ConfirmationPanel(CSSim cssim, ParameterSet parameterSet) {
		this.parent = cssim;
		
		setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
		
		JPanel panel = new JPanel();
		add(panel);
		setBounds(0, 0, 450, 420);
		panel.setLayout(new BorderLayout(0, 0));
		
		parameterPanel = new ReadOnlyParameterSubPanel(parameterSet);
		panel.add(parameterPanel, BorderLayout.CENTER);
		
		JPanel controlPanel = new JPanel();
		panel.add(controlPanel, BorderLayout.SOUTH);
		controlPanel.setLayout(new FlowLayout(FlowLayout.RIGHT, 5, 5));
		
		JButton btnSimulateWithTheseParameters = new JButton("Confirm & Simulate");
		controlPanel.add(btnSimulateWithTheseParameters);
		
		JButton btnBack = new JButton("Back");
		controlPanel.add(btnBack);
		btnBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent se) {
				parent.onBackButtonClick();
			}
		});
		btnSimulateWithTheseParameters.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				parent.onSimulationButtonClick();
			}
		});
	}
}
