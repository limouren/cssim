package hk.hku.cs.csis0297.cssim.gui;

import hk.hku.cs.csis0297.cssim.ParameterSet;

import java.awt.Color;
import java.awt.Component;

import javax.swing.JSpinner;
import javax.swing.JTextField;

@SuppressWarnings("serial")
public class ReadOnlyParameterSubPanel extends ParameterSubPanel {
	
	Component[] paramterComponents = new Component[] {
			spinnerPause,
			spinnerInitialQueueLength,
			spinnerMaxQueueLength,
			spinnerBothFoodAndDrink,
			spinnerDrinkOnly,
			spinnerCustomFoodOnly,
			spinnerPrepackFoodOnly,
			spinnerCustomerMeanRate,
			spinnerCookMaxServTime,
			spinnerBaristaMaxServTime,
			spinnerCashierMaxServTime,
			spinnerCashierMinServTime,
			spinnerBaristaMinServTime,
			spinnerCookMinServTime,
			spinnerCookNumber,
			spinnerBaristaNumber,
			spinnerCashierNumber,
			textFieldStartTime,
			textFieldEndTime,
	};
		
	public ReadOnlyParameterSubPanel(ParameterSet parameterSet) {
		super();
		
		for (Component component : paramterComponents) {
			component.setEnabled(false);
			
			if (component instanceof JSpinner) {
				((JSpinner.DefaultEditor)((JSpinner) component).getEditor()).getTextField().setDisabledTextColor(Color.BLUE);
			} else if (component instanceof JTextField) {
				((JTextField) component).setDisabledTextColor(Color.BLUE);
			}
		}
		
		setParameterSet(parameterSet);
	}
}
