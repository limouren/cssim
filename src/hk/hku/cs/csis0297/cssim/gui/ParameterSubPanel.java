package hk.hku.cs.csis0297.cssim.gui;

import hk.hku.cs.csis0297.cssim.ParameterSet;
import hk.hku.cs.csis0297.cssim.domain.Customer;
import hk.hku.cs.csis0297.cssim.domain.Queue;
import hk.hku.cs.csis0297.cssim.domain.Time;
import hk.hku.cs.csis0297.cssim.io.ParameterPersister;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.io.FileNotFoundException;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import javax.swing.BoxLayout;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.SpringLayout;
import javax.swing.UIManager;
import javax.swing.border.TitledBorder;

@SuppressWarnings("serial")
public class ParameterSubPanel extends JPanel {
	protected JSpinner spinnerPause;
	protected JSpinner spinnerInitialQueueLength;
	protected JSpinner spinnerMaxQueueLength;
	protected JSpinner spinnerBothFoodAndDrink;
	protected JSpinner spinnerDrinkOnly;
	protected JSpinner spinnerCustomFoodOnly;
	protected JSpinner spinnerPrepackFoodOnly;
	protected JSpinner spinnerCustomerMeanRate;
	protected JSpinner spinnerCookMaxServTime;
	protected JSpinner spinnerBaristaMaxServTime;
	protected JSpinner spinnerCashierMaxServTime;
	protected JSpinner spinnerCashierMinServTime;
	protected JSpinner spinnerBaristaMinServTime;
	protected JSpinner spinnerCookMinServTime;
	protected JSpinner spinnerCookNumber;
	protected JSpinner spinnerBaristaNumber;
	protected JSpinner spinnerCashierNumber;
	protected JFormattedTextField textFieldStartTime;
	protected JFormattedTextField textFieldEndTime;

	private ParameterPersister parameterPersister = new ParameterPersister();
	private static SimpleDateFormat timeFormat = new SimpleDateFormat("kk:mm");
	
	/**
	 * Create the panel.
	 */
	public ParameterSubPanel() {
		setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
		
		JPanel panel = new JPanel();
		add(panel);
		SpringLayout sl_panel = new SpringLayout();
		panel.setLayout(sl_panel);
		
		JPanel staffSettingPanel = new JPanel();
		sl_panel.putConstraint(SpringLayout.NORTH, staffSettingPanel, 13, SpringLayout.NORTH, panel);
		sl_panel.putConstraint(SpringLayout.WEST, staffSettingPanel, 10, SpringLayout.WEST, panel);
		sl_panel.putConstraint(SpringLayout.SOUTH, staffSettingPanel, 135, SpringLayout.NORTH, panel);
		sl_panel.putConstraint(SpringLayout.EAST, staffSettingPanel, -12, SpringLayout.EAST, panel);
		staffSettingPanel.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Staff Settings", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel.add(staffSettingPanel);
		GridBagLayout gbl_staffSettingPanel = new GridBagLayout();
		gbl_staffSettingPanel.columnWidths = new int[]{0, 0, 0, 0, 0};
		gbl_staffSettingPanel.rowHeights = new int[]{0, 0, 0, 0, 0};
		gbl_staffSettingPanel.columnWeights = new double[]{1.0, 1.0, 1.0, 1.0, Double.MIN_VALUE};
		gbl_staffSettingPanel.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		staffSettingPanel.setLayout(gbl_staffSettingPanel);
		
		JLabel lblNumber = new JLabel("Number");
		GridBagConstraints gbc_lblNumber = new GridBagConstraints();
		gbc_lblNumber.insets = new Insets(0, 0, 5, 5);
		gbc_lblNumber.gridx = 1;
		gbc_lblNumber.gridy = 0;
		staffSettingPanel.add(lblNumber, gbc_lblNumber);
		
		JLabel lblMinServTime = new JLabel("Min Serv. Time");
		GridBagConstraints gbc_lblMinServTime = new GridBagConstraints();
		gbc_lblMinServTime.insets = new Insets(0, 0, 5, 5);
		gbc_lblMinServTime.gridx = 2;
		gbc_lblMinServTime.gridy = 0;
		staffSettingPanel.add(lblMinServTime, gbc_lblMinServTime);
		
		JLabel lblMaxServTime = new JLabel("Max Serv. Time");
		GridBagConstraints gbc_lblMaxServTime = new GridBagConstraints();
		gbc_lblMaxServTime.insets = new Insets(0, 0, 5, 0);
		gbc_lblMaxServTime.gridx = 3;
		gbc_lblMaxServTime.gridy = 0;
		staffSettingPanel.add(lblMaxServTime, gbc_lblMaxServTime);
		
		JLabel lblCashier = new JLabel("Cashier");
		GridBagConstraints gbc_lblCashier = new GridBagConstraints();
		gbc_lblCashier.anchor = GridBagConstraints.WEST;
		gbc_lblCashier.insets = new Insets(0, 0, 5, 5);
		gbc_lblCashier.gridx = 0;
		gbc_lblCashier.gridy = 1;
		staffSettingPanel.add(lblCashier, gbc_lblCashier);
		
		spinnerCashierNumber = new JSpinner();
		spinnerCashierNumber.setModel(new SpinnerNumberModel(new Integer(1), new Integer(1), null, new Integer(1)));
		GridBagConstraints gbc_spinnerCashierNumber = new GridBagConstraints();
		gbc_spinnerCashierNumber.fill = GridBagConstraints.HORIZONTAL;
		gbc_spinnerCashierNumber.insets = new Insets(0, 0, 5, 5);
		gbc_spinnerCashierNumber.gridx = 1;
		gbc_spinnerCashierNumber.gridy = 1;
		staffSettingPanel.add(spinnerCashierNumber, gbc_spinnerCashierNumber);
		
		spinnerCashierMinServTime = new JSpinner();
		spinnerCashierMinServTime.setModel(new SpinnerNumberModel(new Integer(1), new Integer(1), null, new Integer(1)));
		GridBagConstraints gbc_spinnerCashierMinServTime = new GridBagConstraints();
		gbc_spinnerCashierMinServTime.fill = GridBagConstraints.HORIZONTAL;
		gbc_spinnerCashierMinServTime.insets = new Insets(0, 0, 5, 5);
		gbc_spinnerCashierMinServTime.gridx = 2;
		gbc_spinnerCashierMinServTime.gridy = 1;
		staffSettingPanel.add(spinnerCashierMinServTime, gbc_spinnerCashierMinServTime);
		
		spinnerCashierMaxServTime = new JSpinner();
		spinnerCashierMaxServTime.setModel(new SpinnerNumberModel(new Integer(1), new Integer(1), null, new Integer(1)));
		GridBagConstraints gbc_spinnerCashierMaxServTime = new GridBagConstraints();
		gbc_spinnerCashierMaxServTime.fill = GridBagConstraints.HORIZONTAL;
		gbc_spinnerCashierMaxServTime.insets = new Insets(0, 0, 5, 0);
		gbc_spinnerCashierMaxServTime.gridx = 3;
		gbc_spinnerCashierMaxServTime.gridy = 1;
		staffSettingPanel.add(spinnerCashierMaxServTime, gbc_spinnerCashierMaxServTime);
		
		JLabel lblBarista = new JLabel("Barista");
		GridBagConstraints gbc_lblBarista = new GridBagConstraints();
		gbc_lblBarista.anchor = GridBagConstraints.WEST;
		gbc_lblBarista.insets = new Insets(0, 0, 5, 5);
		gbc_lblBarista.gridx = 0;
		gbc_lblBarista.gridy = 2;
		staffSettingPanel.add(lblBarista, gbc_lblBarista);
		
		spinnerBaristaNumber = new JSpinner();
		spinnerBaristaNumber.setModel(new SpinnerNumberModel(new Integer(1), new Integer(1), null, new Integer(1)));
		GridBagConstraints gbc_spinnerBaristaNumber = new GridBagConstraints();
		gbc_spinnerBaristaNumber.fill = GridBagConstraints.HORIZONTAL;
		gbc_spinnerBaristaNumber.insets = new Insets(0, 0, 5, 5);
		gbc_spinnerBaristaNumber.gridx = 1;
		gbc_spinnerBaristaNumber.gridy = 2;
		staffSettingPanel.add(spinnerBaristaNumber, gbc_spinnerBaristaNumber);
		
		spinnerBaristaMinServTime = new JSpinner();
		spinnerBaristaMinServTime.setModel(new SpinnerNumberModel(new Integer(1), new Integer(1), null, new Integer(1)));
		GridBagConstraints gbc_spinnerBaristaMinServTime = new GridBagConstraints();
		gbc_spinnerBaristaMinServTime.fill = GridBagConstraints.HORIZONTAL;
		gbc_spinnerBaristaMinServTime.insets = new Insets(0, 0, 5, 5);
		gbc_spinnerBaristaMinServTime.gridx = 2;
		gbc_spinnerBaristaMinServTime.gridy = 2;
		staffSettingPanel.add(spinnerBaristaMinServTime, gbc_spinnerBaristaMinServTime);
		
		spinnerBaristaMaxServTime = new JSpinner();
		spinnerBaristaMaxServTime.setModel(new SpinnerNumberModel(new Integer(1), new Integer(1), null, new Integer(1)));
		GridBagConstraints gbc_spinnerBaristaMaxServTime = new GridBagConstraints();
		gbc_spinnerBaristaMaxServTime.fill = GridBagConstraints.HORIZONTAL;
		gbc_spinnerBaristaMaxServTime.insets = new Insets(0, 0, 5, 0);
		gbc_spinnerBaristaMaxServTime.gridx = 3;
		gbc_spinnerBaristaMaxServTime.gridy = 2;
		staffSettingPanel.add(spinnerBaristaMaxServTime, gbc_spinnerBaristaMaxServTime);
		
		JLabel lblCook = new JLabel("Cook");
		GridBagConstraints gbc_lblCook = new GridBagConstraints();
		gbc_lblCook.anchor = GridBagConstraints.WEST;
		gbc_lblCook.insets = new Insets(0, 0, 0, 5);
		gbc_lblCook.gridx = 0;
		gbc_lblCook.gridy = 3;
		staffSettingPanel.add(lblCook, gbc_lblCook);
		
		spinnerCookNumber = new JSpinner();
		spinnerCookNumber.setModel(new SpinnerNumberModel(new Integer(1), new Integer(1), null, new Integer(1)));
		GridBagConstraints gbc_spinnerCookNumber = new GridBagConstraints();
		gbc_spinnerCookNumber.fill = GridBagConstraints.HORIZONTAL;
		gbc_spinnerCookNumber.insets = new Insets(0, 0, 0, 5);
		gbc_spinnerCookNumber.gridx = 1;
		gbc_spinnerCookNumber.gridy = 3;
		staffSettingPanel.add(spinnerCookNumber, gbc_spinnerCookNumber);
		
		spinnerCookMinServTime = new JSpinner();
		spinnerCookMinServTime.setModel(new SpinnerNumberModel(new Integer(1), new Integer(1), null, new Integer(1)));
		GridBagConstraints gbc_spinnerCookMinServTime = new GridBagConstraints();
		gbc_spinnerCookMinServTime.fill = GridBagConstraints.HORIZONTAL;
		gbc_spinnerCookMinServTime.insets = new Insets(0, 0, 0, 5);
		gbc_spinnerCookMinServTime.gridx = 2;
		gbc_spinnerCookMinServTime.gridy = 3;
		staffSettingPanel.add(spinnerCookMinServTime, gbc_spinnerCookMinServTime);
		
		spinnerCookMaxServTime = new JSpinner();
		spinnerCookMaxServTime.setModel(new SpinnerNumberModel(new Integer(1), new Integer(1), null, new Integer(1)));
		GridBagConstraints gbc_spinnerCookMaxServTime = new GridBagConstraints();
		gbc_spinnerCookMaxServTime.fill = GridBagConstraints.HORIZONTAL;
		gbc_spinnerCookMaxServTime.gridx = 3;
		gbc_spinnerCookMaxServTime.gridy = 3;
		staffSettingPanel.add(spinnerCookMaxServTime, gbc_spinnerCookMaxServTime);
		
		JPanel customerPanel = new JPanel();
		sl_panel.putConstraint(SpringLayout.NORTH, customerPanel, 6, SpringLayout.SOUTH, staffSettingPanel);
		sl_panel.putConstraint(SpringLayout.WEST, customerPanel, 10, SpringLayout.WEST, panel);
		sl_panel.putConstraint(SpringLayout.EAST, customerPanel, 0, SpringLayout.EAST, staffSettingPanel);
		panel.add(customerPanel);
		customerPanel.setLayout(new BoxLayout(customerPanel, BoxLayout.X_AXIS));
		
		JPanel customerDistributionPanel = new JPanel();
		customerDistributionPanel.setMaximumSize(new Dimension(300, 32767));
		customerDistributionPanel.setBorder(new TitledBorder(null, "Customer Distribution", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		customerPanel.add(customerDistributionPanel);
		GridBagLayout gbl_customerDistributionPanel = new GridBagLayout();
		gbl_customerDistributionPanel.columnWidths = new int[]{0, 0, 0, 0};
		gbl_customerDistributionPanel.rowHeights = new int[]{0, 0, 0, 0, 0};
		gbl_customerDistributionPanel.columnWeights = new double[]{0.0, 0.0, 0.0, Double.MIN_VALUE};
		gbl_customerDistributionPanel.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		customerDistributionPanel.setLayout(gbl_customerDistributionPanel);
		
		JLabel lblPrepackFoodOnly = new JLabel("Prepack food only");
		GridBagConstraints gbc_lblPrepackFoodOnly = new GridBagConstraints();
		gbc_lblPrepackFoodOnly.anchor = GridBagConstraints.WEST;
		gbc_lblPrepackFoodOnly.insets = new Insets(0, 0, 5, 5);
		gbc_lblPrepackFoodOnly.gridx = 0;
		gbc_lblPrepackFoodOnly.gridy = 0;
		customerDistributionPanel.add(lblPrepackFoodOnly, gbc_lblPrepackFoodOnly);
		
		spinnerPrepackFoodOnly = new JSpinner();
		lblPrepackFoodOnly.setLabelFor(spinnerPrepackFoodOnly);
		spinnerPrepackFoodOnly.setModel(new SpinnerNumberModel(25, 0, 100, 1));
		GridBagConstraints gbc_spinnerPrepackFoodOnly = new GridBagConstraints();
		gbc_spinnerPrepackFoodOnly.insets = new Insets(0, 0, 5, 5);
		gbc_spinnerPrepackFoodOnly.gridx = 1;
		gbc_spinnerPrepackFoodOnly.gridy = 0;
		customerDistributionPanel.add(spinnerPrepackFoodOnly, gbc_spinnerPrepackFoodOnly);
		
		JLabel label = new JLabel("%");
		GridBagConstraints gbc_label = new GridBagConstraints();
		gbc_label.insets = new Insets(0, 0, 5, 0);
		gbc_label.gridx = 2;
		gbc_label.gridy = 0;
		customerDistributionPanel.add(label, gbc_label);
		
		JLabel lblCustomFoodOnly = new JLabel("Custom food only");
		GridBagConstraints gbc_lblCustomFoodOnly = new GridBagConstraints();
		gbc_lblCustomFoodOnly.anchor = GridBagConstraints.WEST;
		gbc_lblCustomFoodOnly.insets = new Insets(0, 0, 5, 5);
		gbc_lblCustomFoodOnly.gridx = 0;
		gbc_lblCustomFoodOnly.gridy = 1;
		customerDistributionPanel.add(lblCustomFoodOnly, gbc_lblCustomFoodOnly);
		
		spinnerCustomFoodOnly = new JSpinner();
		lblCustomFoodOnly.setLabelFor(spinnerCustomFoodOnly);
		spinnerCustomFoodOnly.setModel(new SpinnerNumberModel(25, 0, 100, 1));
		GridBagConstraints gbc_spinnerCustomFoodOnly = new GridBagConstraints();
		gbc_spinnerCustomFoodOnly.insets = new Insets(0, 0, 5, 5);
		gbc_spinnerCustomFoodOnly.gridx = 1;
		gbc_spinnerCustomFoodOnly.gridy = 1;
		customerDistributionPanel.add(spinnerCustomFoodOnly, gbc_spinnerCustomFoodOnly);
		
		JLabel label_1 = new JLabel("%");
		GridBagConstraints gbc_label_1 = new GridBagConstraints();
		gbc_label_1.insets = new Insets(0, 0, 5, 0);
		gbc_label_1.gridx = 2;
		gbc_label_1.gridy = 1;
		customerDistributionPanel.add(label_1, gbc_label_1);
		
		JLabel lblDrinkOnly = new JLabel("Drink only");
		GridBagConstraints gbc_lblDrinkOnly = new GridBagConstraints();
		gbc_lblDrinkOnly.anchor = GridBagConstraints.WEST;
		gbc_lblDrinkOnly.insets = new Insets(0, 0, 5, 5);
		gbc_lblDrinkOnly.gridx = 0;
		gbc_lblDrinkOnly.gridy = 2;
		customerDistributionPanel.add(lblDrinkOnly, gbc_lblDrinkOnly);
		
		spinnerDrinkOnly = new JSpinner();
		lblDrinkOnly.setLabelFor(spinnerDrinkOnly);
		spinnerDrinkOnly.setModel(new SpinnerNumberModel(25, 0, 100, 1));
		GridBagConstraints gbc_spinnerDrinkOnly = new GridBagConstraints();
		gbc_spinnerDrinkOnly.insets = new Insets(0, 0, 5, 5);
		gbc_spinnerDrinkOnly.gridx = 1;
		gbc_spinnerDrinkOnly.gridy = 2;
		customerDistributionPanel.add(spinnerDrinkOnly, gbc_spinnerDrinkOnly);
		
		JLabel label_2 = new JLabel("%");
		GridBagConstraints gbc_label_2 = new GridBagConstraints();
		gbc_label_2.insets = new Insets(0, 0, 5, 0);
		gbc_label_2.gridx = 2;
		gbc_label_2.gridy = 2;
		customerDistributionPanel.add(label_2, gbc_label_2);
		
		JLabel lblBothFoodAnd = new JLabel("Both food and drink");
		GridBagConstraints gbc_lblBothFoodAnd = new GridBagConstraints();
		gbc_lblBothFoodAnd.insets = new Insets(0, 0, 0, 5);
		gbc_lblBothFoodAnd.anchor = GridBagConstraints.WEST;
		gbc_lblBothFoodAnd.gridx = 0;
		gbc_lblBothFoodAnd.gridy = 3;
		customerDistributionPanel.add(lblBothFoodAnd, gbc_lblBothFoodAnd);
		
		spinnerBothFoodAndDrink = new JSpinner();
		lblBothFoodAnd.setLabelFor(spinnerBothFoodAndDrink);
		spinnerBothFoodAndDrink.setModel(new SpinnerNumberModel(25, 0, 100, 1));
		GridBagConstraints gbc_spinnerBothFoodAndDrink = new GridBagConstraints();
		gbc_spinnerBothFoodAndDrink.insets = new Insets(0, 0, 0, 5);
		gbc_spinnerBothFoodAndDrink.gridx = 1;
		gbc_spinnerBothFoodAndDrink.gridy = 3;
		customerDistributionPanel.add(spinnerBothFoodAndDrink, gbc_spinnerBothFoodAndDrink);
		
		JLabel label_3 = new JLabel("%");
		GridBagConstraints gbc_label_3 = new GridBagConstraints();
		gbc_label_3.gridx = 2;
		gbc_label_3.gridy = 3;
		customerDistributionPanel.add(label_3, gbc_label_3);
		
		JPanel customerRatePanel = new JPanel();
		FlowLayout flowLayout = (FlowLayout) customerRatePanel.getLayout();
		flowLayout.setAlignment(FlowLayout.LEFT);
		customerRatePanel.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Customer Arrival Rate", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		customerPanel.add(customerRatePanel);
		
		JLabel lblMeanRate = new JLabel("Mean:");
		customerRatePanel.add(lblMeanRate);
		
		spinnerCustomerMeanRate = new JSpinner();
		spinnerCustomerMeanRate.setPreferredSize(new Dimension(40, 22));
		spinnerCustomerMeanRate.setModel(new SpinnerNumberModel(new Double(10), new Double(0), null, new Double(1)));
		customerRatePanel.add(spinnerCustomerMeanRate);
		
		JLabel lblmin = new JLabel("/min");
		customerRatePanel.add(lblmin);
		
		JPanel panelSimulationTime = new JPanel();
		sl_panel.putConstraint(SpringLayout.NORTH, panelSimulationTime, 6, SpringLayout.SOUTH, customerPanel);
		sl_panel.putConstraint(SpringLayout.WEST, panelSimulationTime, 10, SpringLayout.WEST, panel);
		sl_panel.putConstraint(SpringLayout.SOUTH, panelSimulationTime, 111, SpringLayout.SOUTH, customerPanel);
		sl_panel.putConstraint(SpringLayout.EAST, panelSimulationTime, 212, SpringLayout.WEST, panel);
		panelSimulationTime.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Simulation Time", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel.add(panelSimulationTime);
		SpringLayout sl_panelSimulationTime = new SpringLayout();
		panelSimulationTime.setLayout(sl_panelSimulationTime);
		
		JLabel lblTo = new JLabel("to");
		sl_panelSimulationTime.putConstraint(SpringLayout.WEST, lblTo, 0, SpringLayout.WEST, panelSimulationTime);
		panelSimulationTime.add(lblTo);
		
		JLabel lblPauseEvery = new JLabel("pause every");
		sl_panelSimulationTime.putConstraint(SpringLayout.WEST, lblPauseEvery, 0, SpringLayout.WEST, panelSimulationTime);
		panelSimulationTime.add(lblPauseEvery);
		
		spinnerPause = new JSpinner();
		sl_panelSimulationTime.putConstraint(SpringLayout.NORTH, spinnerPause, 56, SpringLayout.NORTH, panelSimulationTime);
		sl_panelSimulationTime.putConstraint(SpringLayout.WEST, spinnerPause, 6, SpringLayout.EAST, lblPauseEvery);
		sl_panelSimulationTime.putConstraint(SpringLayout.NORTH, lblPauseEvery, 3, SpringLayout.NORTH, spinnerPause);
		spinnerPause.setModel(new SpinnerNumberModel(new Integer(5), new Integer(0), null, new Integer(1)));
		panelSimulationTime.add(spinnerPause);
		
		JLabel lblMin = new JLabel("min");
		sl_panelSimulationTime.putConstraint(SpringLayout.NORTH, lblMin, 0, SpringLayout.NORTH, lblPauseEvery);
		sl_panelSimulationTime.putConstraint(SpringLayout.WEST, lblMin, 5, SpringLayout.EAST, spinnerPause);
		panelSimulationTime.add(lblMin);
		
		JPanel Queue = new JPanel();
		sl_panel.putConstraint(SpringLayout.NORTH, Queue, 6, SpringLayout.SOUTH, customerPanel);
		sl_panel.putConstraint(SpringLayout.WEST, Queue, 6, SpringLayout.EAST, panelSimulationTime);
		sl_panel.putConstraint(SpringLayout.SOUTH, Queue, 0, SpringLayout.SOUTH, panelSimulationTime);
		
		textFieldStartTime = new JFormattedTextField(timeFormat);
		sl_panelSimulationTime.putConstraint(SpringLayout.NORTH, textFieldStartTime, 0, SpringLayout.NORTH, panelSimulationTime);
		sl_panelSimulationTime.putConstraint(SpringLayout.EAST, textFieldStartTime, -42, SpringLayout.EAST, panelSimulationTime);
		setFormattedTimeField(textFieldStartTime, Time.parseTime("09:00"));
		panelSimulationTime.add(textFieldStartTime);
		textFieldStartTime.setColumns(10);
		
		textFieldEndTime = new JFormattedTextField(timeFormat);
		sl_panelSimulationTime.putConstraint(SpringLayout.SOUTH, textFieldStartTime, -6, SpringLayout.NORTH, textFieldEndTime);
		sl_panelSimulationTime.putConstraint(SpringLayout.WEST, textFieldEndTime, 21, SpringLayout.EAST, lblTo);
		sl_panelSimulationTime.putConstraint(SpringLayout.SOUTH, textFieldEndTime, 50, SpringLayout.NORTH, panelSimulationTime);
		sl_panelSimulationTime.putConstraint(SpringLayout.NORTH, textFieldEndTime, 28, SpringLayout.NORTH, panelSimulationTime);
		sl_panelSimulationTime.putConstraint(SpringLayout.EAST, textFieldEndTime, -42, SpringLayout.EAST, panelSimulationTime);
		setFormattedTimeField(textFieldEndTime, Time.parseTime("21:00"));
		panelSimulationTime.add(textFieldEndTime);
		textFieldEndTime.setColumns(10);
		
		JLabel lblFrom = new JLabel("from");
		sl_panelSimulationTime.putConstraint(SpringLayout.WEST, textFieldStartTime, 5, SpringLayout.EAST, lblFrom);
		sl_panelSimulationTime.putConstraint(SpringLayout.NORTH, lblTo, 12, SpringLayout.SOUTH, lblFrom);
		sl_panelSimulationTime.putConstraint(SpringLayout.NORTH, lblFrom, 3, SpringLayout.NORTH, textFieldStartTime);
		sl_panelSimulationTime.putConstraint(SpringLayout.WEST, lblFrom, 0, SpringLayout.WEST, lblTo);
		panelSimulationTime.add(lblFrom);
		sl_panel.putConstraint(SpringLayout.EAST, Queue, -12, SpringLayout.EAST, panel);
		Queue.setBorder(new TitledBorder(null, "Queue", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel.add(Queue);
		SpringLayout sl_Queue = new SpringLayout();
		Queue.setLayout(sl_Queue);
		
		JLabel lblInitialQueueLength = new JLabel("Initial queue length:");
		sl_Queue.putConstraint(SpringLayout.NORTH, lblInitialQueueLength, 3, SpringLayout.NORTH, Queue);
		Queue.add(lblInitialQueueLength);
		
		spinnerInitialQueueLength = new JSpinner();
		spinnerInitialQueueLength.setModel(new SpinnerNumberModel(new Integer(0), new Integer(0), null, new Integer(1)));
		sl_Queue.putConstraint(SpringLayout.NORTH, spinnerInitialQueueLength, 0, SpringLayout.NORTH, Queue);
		sl_Queue.putConstraint(SpringLayout.WEST, spinnerInitialQueueLength, 6, SpringLayout.EAST, lblInitialQueueLength);
		spinnerInitialQueueLength.setPreferredSize(new Dimension(50, 22));
		Queue.add(spinnerInitialQueueLength);
		
		JLabel lblMaxQueueLength = new JLabel("Max. queue length:");
		sl_Queue.putConstraint(SpringLayout.WEST, lblInitialQueueLength, 0, SpringLayout.WEST, lblMaxQueueLength);
		sl_Queue.putConstraint(SpringLayout.NORTH, lblMaxQueueLength, 31, SpringLayout.NORTH, Queue);
		sl_Queue.putConstraint(SpringLayout.WEST, lblMaxQueueLength, 0, SpringLayout.WEST, Queue);
		Queue.add(lblMaxQueueLength);
		
		spinnerMaxQueueLength = new JSpinner();
		sl_Queue.putConstraint(SpringLayout.EAST, spinnerMaxQueueLength, 0, SpringLayout.EAST, spinnerInitialQueueLength);
		spinnerMaxQueueLength.setPreferredSize(new Dimension(50, 22));
		sl_Queue.putConstraint(SpringLayout.WEST, spinnerMaxQueueLength, 10, SpringLayout.EAST, lblMaxQueueLength);
		sl_Queue.putConstraint(SpringLayout.NORTH, spinnerMaxQueueLength, 28, SpringLayout.NORTH, Queue);
		spinnerMaxQueueLength.setModel(new SpinnerNumberModel(new Integer(100), new Integer(1), null, new Integer(1)));
		Queue.add(spinnerMaxQueueLength);

		loadDefaultParameterSet();
	}
	
	// load saved ParameterSet or configured default one
	protected void loadDefaultParameterSet() {
		ParameterSet parameterSet = null;
		try {
			parameterSet = parameterPersister.read();
		} catch (FileNotFoundException e) {
			// instantize a new parameter if no pre-existing default 
			parameterSet = new ParameterSet();
			
			// being lazy
			parameterSet.orderPattern[Customer.FOOD_AND_DRINK] -= parameterSet.orderPattern[Customer.FOOD];
			parameterSet.orderPattern[Customer.FOOD] -= parameterSet.orderPattern[Customer.DRINK];
			parameterSet.orderPattern[Customer.DRINK] -= parameterSet.orderPattern[Customer.PREPACKED_ITEM];
		}
		
		setParameterSet(parameterSet);
	}
	
	// set parameter value to the fields
	protected void setParameterSet(ParameterSet parameterSet) {
		spinnerCashierNumber.setValue(parameterSet.numOfStaff[Queue.CASHIER]);
		spinnerCashierMinServTime.setValue(parameterSet.minServiceTime[Queue.CASHIER]);
		spinnerCashierMaxServTime.setValue(parameterSet.maxServiceTime[Queue.CASHIER]);
		
		spinnerBaristaNumber.setValue(parameterSet.numOfStaff[Queue.BARISTA]);
		spinnerBaristaMinServTime.setValue(parameterSet.minServiceTime[Queue.BARISTA]);
		spinnerBaristaMaxServTime.setValue(parameterSet.maxServiceTime[Queue.BARISTA]);
		
		spinnerCookNumber.setValue(parameterSet.numOfStaff[Queue.COOK]);
		spinnerCookMinServTime.setValue(parameterSet.minServiceTime[Queue.COOK]);
		spinnerCookMaxServTime.setValue(parameterSet.maxServiceTime[Queue.COOK]);
		
		spinnerPrepackFoodOnly.setValue(parameterSet.orderPattern[Customer.PREPACKED_ITEM]);
		spinnerDrinkOnly.setValue(parameterSet.orderPattern[Customer.DRINK]);
		spinnerCustomFoodOnly.setValue(parameterSet.orderPattern[Customer.FOOD]);
		spinnerBothFoodAndDrink.setValue(parameterSet.orderPattern[Customer.FOOD_AND_DRINK]);
		
		spinnerCustomerMeanRate.setValue(parameterSet.customerRate);
	
		setFormattedTimeField(textFieldStartTime, new Time(parameterSet.startTime));
		setFormattedTimeField(textFieldEndTime, new Time(parameterSet.endTime));
		spinnerPause.setValue(parameterSet.simulationPause);

		spinnerInitialQueueLength.setValue(parameterSet.initialQueueLength);
		spinnerMaxQueueLength.setValue(parameterSet.maxQueueLength);
	}
	
	// get the associated ParameterSet from the Panel
	protected ParameterSet getParameterSet() {
		ParameterSet parameterSet = new ParameterSet();
		
		parameterSet.numOfStaff[Queue.CASHIER] = (Integer) spinnerCashierNumber.getValue();
		parameterSet.minServiceTime[Queue.CASHIER] = (Integer) spinnerCashierMinServTime.getValue();
		parameterSet.maxServiceTime[Queue.CASHIER] = (Integer) spinnerCashierMaxServTime.getValue();

		parameterSet.numOfStaff[Queue.BARISTA] = (Integer) spinnerBaristaNumber.getValue();
		parameterSet.minServiceTime[Queue.BARISTA] = (Integer) spinnerBaristaMinServTime.getValue();
		parameterSet.maxServiceTime[Queue.BARISTA] = (Integer) spinnerBaristaMaxServTime.getValue();

		parameterSet.numOfStaff[Queue.COOK] = (Integer) spinnerCookNumber.getValue();
		parameterSet.minServiceTime[Queue.COOK] = (Integer) spinnerCookMinServTime.getValue();
		parameterSet.maxServiceTime[Queue.COOK] = (Integer) spinnerCookMaxServTime.getValue();

		parameterSet.orderPattern[Customer.PREPACKED_ITEM] = (Integer) spinnerPrepackFoodOnly.getValue();
		parameterSet.orderPattern[Customer.DRINK] = (Integer) spinnerDrinkOnly.getValue();
		parameterSet.orderPattern[Customer.FOOD] = (Integer) spinnerCustomFoodOnly.getValue();
		parameterSet.orderPattern[Customer.FOOD_AND_DRINK] = (Integer) spinnerBothFoodAndDrink.getValue();
		
		parameterSet.customerRate = (Double) spinnerCustomerMeanRate.getValue();
		
		parameterSet.startTime = Time.parseTime(textFieldStartTime.getText()).getTime();
		parameterSet.endTime = Time.parseTime(textFieldEndTime.getText()).getTime();
		parameterSet.simulationPause = (Integer) spinnerPause.getValue();
		
		parameterSet.initialQueueLength = (Integer) spinnerInitialQueueLength.getValue();
		parameterSet.maxQueueLength = (Integer) spinnerMaxQueueLength.getValue();
		
		return parameterSet;
	}
	
	private static void setFormattedTimeField(JFormattedTextField field, Time time) {
		try {
			field.setValue(timeFormat.parse(time.toString()));
		} catch (ParseException e) {
			// it should not happen, if it happens, fail hard
			throw new RuntimeException(String.format("Error occurred while paring time=%s", time.getTime()), e);
		}
	}
}
