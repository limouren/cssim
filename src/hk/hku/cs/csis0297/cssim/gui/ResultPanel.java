package hk.hku.cs.csis0297.cssim.gui;

import hk.hku.cs.csis0297.cssim.Result;
import hk.hku.cs.csis0297.cssim.ResultRecord;
import hk.hku.cs.csis0297.cssim.domain.Customer;
import hk.hku.cs.csis0297.cssim.domain.Queue;
import hk.hku.cs.csis0297.cssim.domain.Time;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.UIManager;

@SuppressWarnings("serial")
public class ResultPanel extends JPanel {
	private JPanel resultPanel;

	protected CSSim parent;
	
	/**
	 * Create the panel.
	 */
	public ResultPanel(CSSim cssim, Result result) {
		parent = cssim;
		
		setLayout(new BorderLayout(0, 0));
		
		JPanel controlPanel = new JPanel();
		add(controlPanel, BorderLayout.SOUTH);
		controlPanel.setLayout(new FlowLayout(FlowLayout.TRAILING, 5, 5));
		
		JButton btnOkay = new JButton("Restart");
		btnOkay.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				parent.onRestartButtonClick();
			}
		});
		btnOkay.setHorizontalAlignment(SwingConstants.RIGHT);
		controlPanel.add(btnOkay);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setPreferredSize(new Dimension(221, 380));
		scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		
		add(scrollPane, BorderLayout.CENTER);
		
		JPanel panel = new JPanel();
		scrollPane.setViewportView(panel);
		panel.setLayout(new BorderLayout(0, 0));
		
		resultPanel = new JPanel();
		panel.add(resultPanel, BorderLayout.CENTER);
		GridBagLayout gbl_resultPanel = new GridBagLayout();
		gbl_resultPanel.columnWidths = new int[]{0, 0, 0, 0};
		gbl_resultPanel.rowHeights = new int[] {0};
		gbl_resultPanel.columnWeights = new double[]{1.0, 1.0, 1.0, Double.MIN_VALUE};
		gbl_resultPanel.rowWeights = new double[]{0.0};
		resultPanel.setLayout(gbl_resultPanel);
		
		JLabel lblTime = new JLabel("Time");
		GridBagConstraints gbc_lblTime = new GridBagConstraints();
		gbc_lblTime.insets = new Insets(0, 0, 0, 5);
		gbc_lblTime.gridx = 0;
		gbc_lblTime.gridy = 0;
		resultPanel.add(lblTime, gbc_lblTime);
		
		JLabel lblQueueLength = new JLabel("Queue 1 Length");
		GridBagConstraints gbc_lblQueueLength = new GridBagConstraints();
		gbc_lblQueueLength.insets = new Insets(0, 0, 0, 5);
		gbc_lblQueueLength.gridx = 1;
		gbc_lblQueueLength.gridy = 0;
		resultPanel.add(lblQueueLength, gbc_lblQueueLength);
		
		JLabel lblQueueLength_1 = new JLabel("Queue 2 Length");
		GridBagConstraints gbc_lblQueueLength_1 = new GridBagConstraints();
		gbc_lblQueueLength_1.gridx = 2;
		gbc_lblQueueLength_1.gridy = 0;
		resultPanel.add(lblQueueLength_1, gbc_lblQueueLength_1);
		
		JTextPane txtpnIAmA = new JTextPane();
		txtpnIAmA.setBackground(UIManager.getColor("Label.background"));
		txtpnIAmA.setText(String.format(
				"Maximum length of the main customer queue: %d\n"
						+ "maximum number of customers waiting for food and drinks: %d\n"
						+ "Average service time of customer who bought\n"
						+ "\ta prepacked item: %.2fmin\n"
						+ "\ta custom-made drink: %.2fmin\n"
						+ "\ta food item: %.2fmin\n"
						+ "\tboth drink and food item: %.2fmin\n"
						+ "Longest service time experienced by a customer: %dmin\n"
						+ "Average percent idle time for\n"
						+ "\tCashier: %.2f%%\n"
						+ "\tBarista: %.2f%%\n"
						+ "\tCook: %.2f%%\n",
				result.getMaxMainQueueLength(),
				result.getMaxFoodDrinkQueueLength(),
				result.getAverageServiceTime()[Customer.PREPACKED_ITEM],
				result.getAverageServiceTime()[Customer.DRINK],
				result.getAverageServiceTime()[Customer.FOOD],
				result.getAverageServiceTime()[Customer.FOOD_AND_DRINK],
				result.getLongestServiceTime(),
				result.getAveragePercentIdleTime()[Queue.CASHIER],
				result.getAveragePercentIdleTime()[Queue.BARISTA],
				result.getAveragePercentIdleTime()[Queue.COOK]
				));
		panel.add(txtpnIAmA, BorderLayout.NORTH);

		// append ResultRecordList to the resultPanel
		appendResult(result);
	}

	protected void appendResult(Result result) {
		GridBagConstraints gbc_resultRow = new GridBagConstraints();
		int rowCount = 1;
		for (ResultRecord row : result.getResultRecordList()) {
			gbc_resultRow.gridy = rowCount;
			
			JLabel time = new JLabel(new Time(row.getTime()).toString());
			gbc_resultRow.gridx = 0;
			resultPanel.add(time, gbc_resultRow);
			
			JLabel mainQueueLength = new JLabel(new Integer(row.getMainQueueLength()).toString());
			gbc_resultRow.gridx = 1;
			resultPanel.add(mainQueueLength, gbc_resultRow);
			
			JLabel foodDrinkQueueLength = new JLabel(new Integer(row.getFoodDrinkQueueLength()).toString());
			gbc_resultRow.gridx = 2;
			resultPanel.add(foodDrinkQueueLength, gbc_resultRow);
			
			++rowCount;
		}
	}
}
