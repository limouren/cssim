package hk.hku.cs.csis0297.cssim.gui;

import hk.hku.cs.csis0297.cssim.ParameterSet;
import hk.hku.cs.csis0297.cssim.io.ParameterPersister;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JPanel;

@SuppressWarnings("serial")
public class ParameterPanel extends JPanel {

	private CSSim parent;
	private ParameterPersister parameterPersister = new ParameterPersister();

	private ParameterSubPanel parameterPanel;
	
	/**
	 * Create the panel.
	 */
	public ParameterPanel(CSSim cssim) {
		this.parent = cssim; 
		
		setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
		
		JPanel panel = new JPanel();
		add(panel);
		setBounds(0, 0, 450, 420);
		panel.setLayout(new BorderLayout(0, 0));
		
		parameterPanel = new ParameterSubPanel();
		panel.add(parameterPanel, BorderLayout.CENTER);
		
		JPanel controlPanel = new JPanel();
		panel.add(controlPanel, BorderLayout.SOUTH);
		controlPanel.setLayout(new FlowLayout(FlowLayout.RIGHT, 5, 5));
		
		JButton btnProceed = new JButton("Proceed");
		controlPanel.add(btnProceed);
		btnProceed.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				parent.onProceedButtonClick();
			}
		});
		
		JButton btnLoadFromDefault = new JButton("Load from default");
		controlPanel.add(btnLoadFromDefault);
		
		JButton btnSaveAsDefault = new JButton("Save as default");
		controlPanel.add(btnSaveAsDefault);
		btnSaveAsDefault.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				try {
					parameterPersister.write(getParameterSet());
				} catch (IOException e) {
					// native handling
					throw new RuntimeException("Cannot write parameter settings.", e);
				}
			}
		});
		btnLoadFromDefault.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				parameterPanel.loadDefaultParameterSet();
			}
		});
	}
	
	// get the associated ParameterSet from the Panel
	protected ParameterSet getParameterSet() {
		return parameterPanel.getParameterSet();
	}
}
