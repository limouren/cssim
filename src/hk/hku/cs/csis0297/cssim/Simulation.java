package hk.hku.cs.csis0297.cssim;

import hk.hku.cs.csis0297.cssim.domain.Customer;
import hk.hku.cs.csis0297.cssim.domain.CustomerComparator;
import hk.hku.cs.csis0297.cssim.domain.DrinkCustomer;
import hk.hku.cs.csis0297.cssim.domain.FoodAndDrinkCustomer;
import hk.hku.cs.csis0297.cssim.domain.FoodCustomer;
import hk.hku.cs.csis0297.cssim.domain.PrepackedItemCustomer;
import hk.hku.cs.csis0297.cssim.domain.Queue;
import hk.hku.cs.csis0297.cssim.domain.Staff;
import hk.hku.cs.csis0297.cssim.log.Log;
import hk.hku.cs.csis0297.cssim.log.LogRecord;
import hk.hku.cs.csis0297.cssim.ultis.Poisson;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

public class Simulation {

	private ParameterSet paramSet;
	private ArrayList<Customer> allCustomers;
	private Queue queueList[];
	private Log log;
	private Result result;

	/*public static void main(String[] args) {
		Simulation sim = new Simulation();
		sim.start();
		sim.getResult();

		sim.displayResult();
		System.out.println("----------");
		sim.displayCustomerData();
		System.out.println("----------");
		sim.displayLog();
	}*/

	public Simulation() {
		paramSet = new ParameterSet();
		allCustomers = new ArrayList<Customer>();
		queueList = new Queue[Queue.NUM_OF_STAFF_CATEGORY];
		log = new Log();
		result = new Result();
	}

	public Simulation(ParameterSet inputParameter) {
		paramSet = inputParameter;
		allCustomers = new ArrayList<Customer>();
		queueList = new Queue[Queue.NUM_OF_STAFF_CATEGORY];
		log = new Log();
		result = new Result();
	}

	public void start() {
		initialCustomer();
		queueList[Queue.CASHIER] = new Queue(allCustomers,
				paramSet.numOfStaff[Queue.CASHIER], paramSet.startTime,
				Queue.CASHIER);
		simulate(queueList[Queue.CASHIER], Queue.CASHIER);

		ArrayList<Customer> drinkCustomers = filterCustomer(Queue.BARISTA,
				Customer.DRINK);
		Collections.sort(drinkCustomers, new CustomerComparator(Queue.BARISTA));
		queueList[Queue.BARISTA] = new Queue(drinkCustomers,
				paramSet.numOfStaff[Queue.BARISTA], paramSet.startTime,
				Queue.BARISTA);
		simulate(queueList[Queue.BARISTA], Queue.BARISTA);

		ArrayList<Customer> foodCustomers = filterCustomer(Queue.COOK,
				Customer.FOOD);
		Collections.sort(foodCustomers, new CustomerComparator(Queue.COOK));
		queueList[Queue.COOK] = new Queue(foodCustomers,
				paramSet.numOfStaff[Queue.COOK], paramSet.startTime, Queue.COOK);
		simulate(queueList[Queue.COOK], Queue.COOK);

		result.generateResult(allCustomers, queueList, paramSet.startTime,
				paramSet.endTime, paramSet.simulationPause,
				paramSet.maxQueueLength, log);
	}

	protected void initialCustomer() {
		/* Add customers according to initial queue length - START */
		for (int i = 0; i < paramSet.initialQueueLength; i++)
			allCustomers.add(generateCustomer(paramSet.startTime,
					allCustomers.size()));
		/* Add customers according to initial queue length - END */

		for (int i = paramSet.startTime; i < paramSet.endTime; i++) {
			int numOfCustomers = Poisson.getInt(paramSet.customerRate);
			for (int j = 1; j <= numOfCustomers; j++)
				allCustomers.add(generateCustomer(i, allCustomers.size()));
		}
	}

	protected Customer generateCustomer(int startTime, int customerId) {
		Customer newCustomer;

		Random rand = new Random();
		int randomNum = rand.nextInt(101); // random number between 0 and 100

		if (randomNum <= paramSet.orderPattern[Customer.PREPACKED_ITEM])
			newCustomer = new PrepackedItemCustomer(startTime, customerId);
		else if (randomNum <= paramSet.orderPattern[Customer.DRINK])
			newCustomer = new DrinkCustomer(startTime, customerId);
		else if (randomNum <= paramSet.orderPattern[Customer.FOOD])
			newCustomer = new FoodCustomer(startTime, customerId);
		else
			newCustomer = new FoodAndDrinkCustomer(startTime, customerId);

		return newCustomer;
	}

	protected ArrayList<Customer> filterCustomer(int staffType, int customerType) {
		ArrayList<Customer> tempCustomerList = new ArrayList<Customer>();

		for (Customer c : allCustomers) {
			if (c.getQueueLeaveTime(Queue.CASHIER) != Customer.NO_RESULT
					&& (c.getOrderType() == customerType || c.getOrderType() == Customer.FOOD_AND_DRINK)) {
				c.setArrivalTime(c.getQueueLeaveTime(Queue.CASHIER), staffType);
				tempCustomerList.add(c);
			}
		}

		return tempCustomerList;
	}

	protected void simulate(Queue currentQueue, int staffType) {

		int shopClock = paramSet.startTime;
		int customerCount = 1;

		/* Log the customer arrival time at this queue - START */
		while (customerCount <= currentQueue.queueLength()) {
			Customer firstCustomer = currentQueue
					.getCustomer(customerCount - 1);
			log.addRecord(firstCustomer.getArrivalTime(staffType),
					firstCustomer.getId(),
					"Arrived at " + currentQueue.getQueueName() + " queue");
			customerCount++;
		}
		/* Log the customer arrival time at this queue - END */

		customerCount = 1;
		while (customerCount <= currentQueue.queueLength()) {
			Customer firstCustomer = currentQueue
					.getCustomer(customerCount - 1);

			if (shopClock < firstCustomer.getArrivalTime(staffType))
				shopClock = firstCustomer.getArrivalTime(staffType);

			if (paramSet.endTime < shopClock)
				break; // halt the simulation since the time is out of range

			/* Select the available staff and increment idle time - START */
			Staff serviceStaff = currentQueue.nextAvailableStaff();

			if (shopClock <= serviceStaff.getServiceEndTime())
				shopClock = serviceStaff.getServiceEndTime();
			else
				serviceStaff.addIdleTime(shopClock
						- serviceStaff.getServiceEndTime());
			/* Select the available staff and increment idle time - END */

			if (paramSet.endTime < shopClock)
				break; // halt the simulation since the time is out of range

			/* Generate service time - START */
			Random rand = new Random();
			int entertainTime = rand.nextInt(paramSet.maxServiceTime[staffType]
					- paramSet.minServiceTime[staffType] + 1)
					+ paramSet.minServiceTime[staffType];
			/* Generate service time - END */

			/* Record the service time and leave queue time - START */
			firstCustomer.setQueueStartEntertainTime(shopClock, staffType);
			log.addRecord(shopClock, firstCustomer.getId(),
					"Started getting served in " + currentQueue.getQueueName()
							+ " queue");

			firstCustomer.setQueueServiceTime(
					(shopClock - firstCustomer.getArrivalTime(staffType))
							+ entertainTime, staffType);
			firstCustomer.setQueueLeaveTime(shopClock + entertainTime,
					staffType);
			log.addRecord(shopClock + entertainTime, firstCustomer.getId(),
					"Got fully served in " + currentQueue.getQueueName()
							+ " queue");

			serviceStaff.setServiceEndTime(shopClock + entertainTime);

			customerCount++;
			/* Record the service time and leave queue time - END */
		}
		currentQueue.addRemainingTimeToIdleTime(paramSet.endTime);
	}

	public Log getLog() {
		return log;
	}

	public Result getResult() {
		return result;
	}

	/* The following functions are for testing only */

	public void displayResult() {
		System.out.println("Time\tMain Queue\tOther Queue");
		for (ResultRecord resultRow : result.getResultRecordList()) {
			System.out.println(resultRow.getTime() / 60 + ":"
					+ String.format("%02d", resultRow.getTime() % 60) + "\t"
					+ resultRow.getMainQueueLength() + "\t"
					+ resultRow.getFoodDrinkQueueLength());
		}

		if (!result.isNoError())
			System.out
					.println("Exceed maximum queue length, simulation stopped!");

		System.out.println("Max main queue length: "
				+ result.getMaxMainQueueLength());
		System.out.println("Max food/drink queue length: "
				+ result.getMaxFoodDrinkQueueLength());

		for (int i = 0; i < Customer.NUM_OF_ORDER_CATEGORY; i++)
			System.out.println("Type " + i + " customer average service time: "
					+ result.getAverageServiceTime()[i]);

		System.out.println("Longest service time: "
				+ result.getLongestServiceTime());

		for (int i = 0; i < Queue.NUM_OF_STAFF_CATEGORY; i++)
			System.out.println("Type " + i
					+ " staff average percent idle time: "
					+ result.getAveragePercentIdleTime()[i] * 100 + "%");
	}

	public void displayCustomerData() {
		System.out
				.println("ID\tORD\tAT-M\tAT-F\tAT-C\tSST-M\tST-M\tST-F\tST-C\tLT-M\tLT-F\tLT-C\tLT-Q2\tLT\tST");
		for (Customer c : allCustomers) {
			System.out.print(c.getId() + "\t");
			System.out.print(c.getOrderType() + "\t");
			System.out.print(c.getArrivalTime(0) + "\t" + c.getArrivalTime(1)
					+ "\t" + c.getArrivalTime(2) + "\t"
					+ c.getQueueStartEntertainTime(0) + "\t"
					+ c.getQueueServiceTime(0) + "\t"
					+ c.getQueueServiceTime(1) + "\t"
					+ c.getQueueServiceTime(2) + "\t" + c.getQueueLeaveTime(0)
					+ "\t" + c.getQueueLeaveTime(1) + "\t"
					+ c.getQueueLeaveTime(2) + "\t"
					+ c.foodDrinkQueueLeaveTime() + "\t" + c.leaveShopTime()
					+ "\t" + c.serviceTime());
			System.out.println();
		}
	}

	public void displayLog() {
		System.out.println("Time\tCustomer ID\tEvent");
		for (LogRecord logRow : log.getLogRecordList())
			if (paramSet.startTime <= logRow.getTime()
					&& logRow.getTime() <= paramSet.endTime)
				System.out.println(logRow.getTime() / 60 + ":"
						+ String.format("%02d", logRow.getTime() % 60) + "\t"
						+ logRow.getCustomerId() + "\t" + logRow.getEvent());
	}
}
