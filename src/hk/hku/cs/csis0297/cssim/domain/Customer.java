package hk.hku.cs.csis0297.cssim.domain;

public abstract class Customer {
	public static final int PREPACKED_ITEM = 0;
	public static final int DRINK = 1;
	public static final int FOOD = 2;
	public static final int FOOD_AND_DRINK = 3;

	public static final int NUM_OF_ORDER_CATEGORY = 4;

	public static final int NO_RESULT = -1;

	protected int id;
	protected int queueArrivalTime[]; // arrival queue time of customer
	protected int queueStartEntertainTime[]; // the time that the customer
												// started being entertained by
												// staff in the queue
	protected int queueServiceTime[]; // the time that the customer got fully
										// served minus arrival queue time of
										// customer
	protected int queueLeaveTime[]; // leave queue time of customer

	protected int orderType;

	public Customer(int arrivalTime, int id) {
		queueArrivalTime = new int[Queue.NUM_OF_STAFF_CATEGORY];
		queueStartEntertainTime = new int[Queue.NUM_OF_STAFF_CATEGORY];
		queueServiceTime = new int[Queue.NUM_OF_STAFF_CATEGORY];
		queueLeaveTime = new int[Queue.NUM_OF_STAFF_CATEGORY];

		for (int i = 0; i < queueArrivalTime.length; i++)
			queueArrivalTime[i] = NO_RESULT;
		for (int i = 0; i < queueStartEntertainTime.length; i++)
			queueStartEntertainTime[i] = NO_RESULT;
		for (int i = 0; i < queueServiceTime.length; i++)
			queueServiceTime[i] = NO_RESULT;
		for (int i = 0; i < queueArrivalTime.length; i++)
			queueLeaveTime[i] = NO_RESULT;

		setArrivalTime(arrivalTime, Queue.CASHIER);

		this.id = id;
	}

	public void setArrivalTime(int arrivalTime, int staffType) {
		queueArrivalTime[staffType] = arrivalTime;
	}

	public int getArrivalTime(int staffType) {
		return queueArrivalTime[staffType];
	}

	public void setQueueStartEntertainTime(int serviceTime, int staffType) {
		queueStartEntertainTime[staffType] = serviceTime;
	}

	public int getQueueStartEntertainTime(int staffType) {
		return queueStartEntertainTime[staffType];
	}

	public void setQueueServiceTime(int serviceTime, int staffType) {
		queueServiceTime[staffType] = serviceTime;
	}

	public int getQueueServiceTime(int staffType) {
		return queueServiceTime[staffType];
	}

	public void setQueueLeaveTime(int leaveTime, int staffType) {
		queueLeaveTime[staffType] = leaveTime;
	}

	public int getQueueLeaveTime(int staffType) {
		return queueLeaveTime[staffType];
	}

	public int getId() {
		return id;
	}

	public int getOrderType() {
		return orderType;
	}

	public abstract int foodDrinkQueueArrivalTime();

	public abstract int foodDrinkQueueLeaveTime();

	public abstract int serviceTime();

	public abstract int leaveShopTime();
}
