package hk.hku.cs.csis0297.cssim.domain;

import java.util.ArrayList;

public class Queue {
	public static final int CASHIER = 0;
	public static final int BARISTA = 1;
	public static final int COOK = 2;

	public static final int NUM_OF_STAFF_CATEGORY = 3;

	private ArrayList<Customer> customerList;
	private ArrayList<Staff> staffList;
	private int queueType;

	public Queue(ArrayList<Customer> customers, int numOfStaff, int startTime,
			int staffType) {
		customerList = new ArrayList<Customer>();
		staffList = new ArrayList<Staff>();
		queueType = staffType;

		for (int i = 0; i < numOfStaff; i++)
			staffList.add(new Staff(startTime));

		for (Customer tempCustomer : customers) {
			if (staffType == Queue.CASHIER)
				customerList.add(tempCustomer);
			else if (staffType == Queue.BARISTA) {
				if (tempCustomer.getOrderType() == Customer.DRINK
						|| tempCustomer.getOrderType() == Customer.FOOD_AND_DRINK)
					customerList.add(tempCustomer);
			} else if (staffType == Queue.COOK) {
				if (tempCustomer.getOrderType() == Customer.FOOD
						|| tempCustomer.getOrderType() == Customer.FOOD_AND_DRINK)
					customerList.add(tempCustomer);
			}
		}
	}

	public Customer getCustomer(int customerIndex) {
		return customerList.get(customerIndex);
	}

	public int queueLength() {
		return customerList.size();
	}

	public Staff nextAvailableStaff() {
		Staff nextStaff = staffList.get(0);

		for (Staff s : staffList) {
			if (s.getServiceEndTime() < nextStaff.getServiceEndTime())
				nextStaff = s;
		}
		return nextStaff;
	}

	public ArrayList<Staff> getStaffList() {
		return staffList;
	}

	public String getQueueName() {
		if (queueType == Queue.CASHIER)
			return "main";
		else if (queueType == Queue.BARISTA)
			return "barista";
		else if (queueType == Queue.COOK)
			return "cook";
		else
			return "unknown";
	}

	public void addRemainingTimeToIdleTime(int endTime) {
		for (Staff s : staffList) {
			if (s.getServiceEndTime() < endTime)
				s.addIdleTime(endTime - s.getServiceEndTime());
		}
	}
}
