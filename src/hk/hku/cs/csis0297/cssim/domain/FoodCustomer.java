package hk.hku.cs.csis0297.cssim.domain;

public class FoodCustomer extends Customer {
	public FoodCustomer(int arrivalTime, int id) {
		super(arrivalTime, id);
		orderType = FOOD;
	}

	@Override
	public int foodDrinkQueueArrivalTime() {
		return queueArrivalTime[Queue.COOK];
	}

	@Override
	public int foodDrinkQueueLeaveTime() {
		return queueLeaveTime[Queue.COOK];
	}

	@Override
	public int serviceTime() {
		if (queueServiceTime[Queue.CASHIER] != NO_RESULT
				&& queueServiceTime[Queue.COOK] != NO_RESULT)
			return queueServiceTime[Queue.CASHIER]
					+ queueServiceTime[Queue.COOK];
		else
			return NO_RESULT;
	}

	@Override
	public int leaveShopTime() {
		return foodDrinkQueueLeaveTime();
	}
}
