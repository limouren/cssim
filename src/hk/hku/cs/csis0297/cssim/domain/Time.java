package hk.hku.cs.csis0297.cssim.domain;

public class Time {
	protected int time;
	
	public Time(int time) {
		if (time < 0) {
			throw new IndexOutOfBoundsException("A Time cannot be negative.");
		}
		
		this.time = time;
	}
	
	public int getTime() {
		return time;
	}
	
	public static Time parseTime(String time) {
		String[] nums = time.split(":");
		
		return new Time(Integer.parseInt(nums[0])*60 + Integer.parseInt(nums[1]));
	}

	// convert time to format in HH:MM
	// TODO: Use StringBuffer for performance
	@Override
	public String toString() {
		String hh = new Integer(time / 60).toString();
		if (hh.length() == 1) {
			hh = "0" + hh;
		}
		
		String mm = new Integer(time % 60).toString();
		if (mm.length() == 1) {
			mm = "0" + mm;
		}
		
		return hh + ":" + mm;
	}
}
