package hk.hku.cs.csis0297.cssim.domain;

public class DrinkCustomer extends Customer {
	public DrinkCustomer(int arrivalTime, int id) {
		super(arrivalTime, id);
		orderType = DRINK;
	}

	@Override
	public int foodDrinkQueueArrivalTime() {
		return queueArrivalTime[Queue.BARISTA];
	}

	@Override
	public int foodDrinkQueueLeaveTime() {
		return queueLeaveTime[Queue.BARISTA];
	}

	@Override
	public int serviceTime() {
		if (queueServiceTime[Queue.CASHIER] != NO_RESULT
				&& queueServiceTime[Queue.BARISTA] != NO_RESULT)
			return queueServiceTime[Queue.CASHIER]
					+ queueServiceTime[Queue.BARISTA];
		else
			return NO_RESULT;
	}

	@Override
	public int leaveShopTime() {
		return foodDrinkQueueLeaveTime();
	}
}
