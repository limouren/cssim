package hk.hku.cs.csis0297.cssim.domain;

public class PrepackedItemCustomer extends Customer {
	public PrepackedItemCustomer(int arrivalTime, int id) {
		super(arrivalTime, id);
		orderType = PREPACKED_ITEM;
	}

	@Override
	public int foodDrinkQueueArrivalTime() {
		return NO_RESULT;
	}

	@Override
	public int foodDrinkQueueLeaveTime() {
		return NO_RESULT;
	}

	@Override
	public int serviceTime() {
		return queueServiceTime[Queue.CASHIER];
	}

	@Override
	public int leaveShopTime() {
		return queueLeaveTime[Queue.CASHIER];
	}
}
