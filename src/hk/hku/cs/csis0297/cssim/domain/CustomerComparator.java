package hk.hku.cs.csis0297.cssim.domain;

import java.util.Comparator;

public class CustomerComparator implements Comparator<Customer> {
	private int staffType;

	public CustomerComparator(int staffType) {
		this.staffType = staffType;
	}

	@Override
	public int compare(Customer customer1, Customer customer2) {
		return customer1.getArrivalTime(staffType)
				- customer2.getArrivalTime(staffType);
	}

}
