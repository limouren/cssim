package hk.hku.cs.csis0297.cssim.domain;

public class FoodAndDrinkCustomer extends Customer {
	public FoodAndDrinkCustomer(int arrivalTime, int id) {
		super(arrivalTime, id);
		orderType = FOOD_AND_DRINK;
	}

	@Override
	public int foodDrinkQueueArrivalTime() {
		if (queueArrivalTime[Queue.BARISTA] != NO_RESULT
				&& queueArrivalTime[Queue.COOK] != NO_RESULT) {
			if (queueArrivalTime[Queue.BARISTA] < queueArrivalTime[Queue.COOK])
				return queueArrivalTime[Queue.BARISTA];
			else
				return queueArrivalTime[Queue.COOK];
		} else
			return NO_RESULT;
	}

	@Override
	public int foodDrinkQueueLeaveTime() {
		if (queueLeaveTime[Queue.BARISTA] != NO_RESULT
				&& queueLeaveTime[Queue.COOK] != NO_RESULT) {
			if (queueLeaveTime[Queue.BARISTA] > queueLeaveTime[Queue.COOK])
				return queueLeaveTime[Queue.BARISTA];
			else
				return queueLeaveTime[Queue.COOK];
		} else
			return NO_RESULT;
	}

	@Override
	public int serviceTime() {
		if (queueServiceTime[Queue.CASHIER] != NO_RESULT
				&& queueServiceTime[Queue.BARISTA] != NO_RESULT
				&& queueServiceTime[Queue.COOK] != NO_RESULT) {
			if (queueServiceTime[Queue.BARISTA] > queueServiceTime[Queue.COOK])
				return queueServiceTime[Queue.CASHIER]
						+ queueServiceTime[Queue.BARISTA];
			else
				return queueServiceTime[Queue.CASHIER]
						+ queueServiceTime[Queue.COOK];
		} else
			return NO_RESULT;
	}

	@Override
	public int leaveShopTime() {
		return foodDrinkQueueLeaveTime();
	}
}
