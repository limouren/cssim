package hk.hku.cs.csis0297.cssim.domain;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class Staff {
	protected int serviceEndTime;

	protected ArrayList<StaffIdleLog> idleRecords;

	public class StaffIdleLog {
		int idleStartTime;
		int idleDuration;

		public StaffIdleLog(int idleStartTime, int idleDuration) {
			this.idleStartTime = idleStartTime;
			this.idleDuration = idleDuration;
		}
	}

	public class StaffIdleLogComparator implements Comparator<StaffIdleLog> {
		@Override
		public int compare(StaffIdleLog log1, StaffIdleLog log2) {
			return log1.idleStartTime - log2.idleStartTime;
		}
	}

	public Staff(int startTime) {
		serviceEndTime = startTime;
		idleRecords = new ArrayList<StaffIdleLog>();
	}

	public int getServiceEndTime() {
		return serviceEndTime;
	}

	public void setServiceEndTime(int time) {
		serviceEndTime = time;
	}

	public void addIdleTime(int idleDuration) {
		idleRecords.add(new StaffIdleLog(serviceEndTime, idleDuration));
	}

	public int getIdleTime() {
		Collections.sort(idleRecords, new StaffIdleLogComparator());
		int idleTime = 0;
		for (StaffIdleLog idleLog : idleRecords)
			idleTime += idleLog.idleDuration;
		return idleTime;
	}

	public int getIdleTime(int endTime) {
		Collections.sort(idleRecords, new StaffIdleLogComparator());

		int idleTime = 0;
		for (StaffIdleLog idleLog : idleRecords) {
			if (idleLog.idleStartTime + idleLog.idleDuration <= endTime)
				idleTime += idleLog.idleDuration;
			else if (idleLog.idleStartTime <= endTime)
				idleTime += endTime - idleLog.idleStartTime;
			else
				break;
		}
		return idleTime;
	}
}
