package hk.hku.cs.csis0297.cssim.log;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;

public class Log {
	private ArrayList<LogRecord> logRecords;

	public Log() {
		logRecords = new ArrayList<LogRecord>();
	}

	public void addRecord(int time, int customerId, String event) {
		logRecords.add(new LogRecord(time, customerId, event));
	}

	protected void sortLogRecord() {
		Collections.sort(logRecords, new LogRecordComparator());
	}

	public void filterLogRecord(int endTime) {
		sortLogRecord();
		ArrayList<LogRecord> filteredRecords = new ArrayList<LogRecord>();

		for (LogRecord log : logRecords) {
			if (log.getTime() <= endTime)
				filteredRecords.add(log);
			else
				break;
		}

		logRecords = filteredRecords;
	}

	public ArrayList<LogRecord> getLogRecordList() {
		sortLogRecord();
		return logRecords;
	}

	// this method simply writes LogRecord in its current order
	public void write(PrintWriter writer) {
		sortLogRecord();
		for (LogRecord record : logRecords) {
			writer.println(record);
		}
	}
}
