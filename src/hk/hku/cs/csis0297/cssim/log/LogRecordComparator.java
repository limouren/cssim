package hk.hku.cs.csis0297.cssim.log;

import java.util.Comparator;

public class LogRecordComparator implements Comparator<LogRecord> {

	@Override
	public int compare(LogRecord log1, LogRecord log2) {
		return log1.getTime() - log2.getTime();
	}

}
