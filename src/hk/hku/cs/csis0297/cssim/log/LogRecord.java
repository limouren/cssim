package hk.hku.cs.csis0297.cssim.log;

import hk.hku.cs.csis0297.cssim.domain.Time;

public class LogRecord {
	private int time;
	private int customerId;
	private String event;

	public LogRecord(int time, int customerId, String event) {
		this.setTime(time);
		this.setCustomerId(customerId);
		this.setEvent(event);
	}

	@Override
	public String toString() {
		return new Time(time).toString() + "," + new Integer(customerId).toString() + "," + event;
	}

	public int getTime() {
		return time;
	}

	public void setTime(int time) {
		this.time = time;
	}

	public int getCustomerId() {
		return customerId;
	}

	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}

	public String getEvent() {
		return event;
	}

	public void setEvent(String event) {
		this.event = event;
	}
}
