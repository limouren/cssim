package hk.hku.cs.csis0297.cssim;

import hk.hku.cs.csis0297.cssim.domain.Customer;
import hk.hku.cs.csis0297.cssim.domain.Queue;

import java.io.Serializable;

public class ParameterSet implements Serializable {
	/**
	 * default serialVersionUID.
	 */
	private static final long serialVersionUID = 6547453201289992057L;

	public int numOfStaff[];
	public int minServiceTime[];
	public int maxServiceTime[];

	public double customerRate;

	public int orderPattern[];

	public int startTime; // in minute, e.g. 9:30 = 9 * 60 + 30
	public int endTime; // in minute, e.g. 17:30 = 17 * 60 + 30

	public int initialQueueLength;
	public int simulationPause;
	public int maxQueueLength;

	public ParameterSet() {
		numOfStaff = new int[Queue.NUM_OF_STAFF_CATEGORY];
		minServiceTime = new int[Queue.NUM_OF_STAFF_CATEGORY];
		maxServiceTime = new int[Queue.NUM_OF_STAFF_CATEGORY];

		numOfStaff[Queue.CASHIER] = 2;
		minServiceTime[Queue.CASHIER] = 1;
		maxServiceTime[Queue.CASHIER] = 2;

		numOfStaff[Queue.BARISTA] = 2;
		minServiceTime[Queue.BARISTA] = 1;
		maxServiceTime[Queue.BARISTA] = 5;

		numOfStaff[Queue.COOK] = 2;
		minServiceTime[Queue.COOK] = 1;
		maxServiceTime[Queue.COOK] = 6;

		customerRate = 3;

		orderPattern = new int[Customer.NUM_OF_ORDER_CATEGORY];

		for (int i = 0; i < orderPattern.length; i++)
			orderPattern[i] = (i + 1) * 25; // evenly distributed proportion of
											// order

		startTime = 7 * 60 + 30; // 7:30
		endTime = 22 * 60 + 0; // 22:00

		initialQueueLength = 0;
		simulationPause = 5;
		maxQueueLength = 50;
	}
}
