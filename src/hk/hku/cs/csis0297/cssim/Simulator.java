package hk.hku.cs.csis0297.cssim;

public class Simulator {
	public Result run(ParameterSet parameterSet) {
		Simulation simulation = new Simulation(parameterSet);
		simulation.start();

		return simulation.getResult();
	}
}
