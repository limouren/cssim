package hk.hku.cs.csis0297.cssim;

public class ResultRecord {
	private int time;
	private int mainQueueLength;
	private int foodDrinkQueueLength;

	ResultRecord(int time, int mainQueueLength, int foodDrinkQueueLength) {
		this.time = time;
		this.mainQueueLength = mainQueueLength;
		this.foodDrinkQueueLength = foodDrinkQueueLength;
	}

	public int getTime() {
		return time;
	}

	public int getMainQueueLength() {
		return mainQueueLength;
	}

	public int getFoodDrinkQueueLength() {
		return foodDrinkQueueLength;
	}
}
